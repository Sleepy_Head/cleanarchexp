﻿using FanControlApp.Infrastructure;

class Program
{
  static void Main(string[] args)
  {

    // Create an instance of the ModbusService with the appropriate COM port
    var modbusMaster = ModbusConfig.CreateRtuMaster("COM4"); // Replace with your COM port
    var fanService = new ModbusService(modbusMaster);

    // Example: Get Max Speed
    var result  = fanService.GetFanMaxSpeed().Result;
    Console.WriteLine($"Max Speed ==== {result.MaxSpeed}");

    // Example: Turn On Fan
    bool isOn = fanService.TurnOn().Result;
    Console.WriteLine($"Fan is {(isOn ? "ON" : "OFF")}");

    //Example: Turn Off Fan
    bool isOff = fanService.TurnOff().Result;
    Console.WriteLine($"Fan is {(isOff ? "ON" : "OFF")}");

  }

}
