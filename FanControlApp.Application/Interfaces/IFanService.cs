using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FanControlApp.Domain;

namespace FanControlApp.Application
{
    public interface IFanService
    {
        Task<Fan> GetFanMaxSpeed();
        Task<bool> TurnOn();
        Task<bool> TurnOff();
        Task<Fan> GetCurrentTemp();
        
    }
}