using FanControlApp.Application;
using FanControlApp.Domain;
using NModbus;

namespace FanControlApp.Infrastructure
{
    //Modbus Service that interacts with the Modbus RTU device
    public class ModbusService : IFanService
    {


        private readonly IModbusMaster _modbusMaster;


        public ModbusService(IModbusMaster modbusMaster)
        {
            this._modbusMaster = modbusMaster;

        }


        public Task<Fan> GetCurrentTemp()
        {
            throw new NotImplementedException();
        }


        public Task<Fan> GetFanMaxSpeed()
        {
            byte slaveId = 1;
            //16388
            ushort startAddress = 16388;
            ushort numberOfPoints = 1;

            ushort[] retrievedValue = _modbusMaster
                .ReadInputRegisters(slaveId, startAddress, numberOfPoints);

            Fan fan = new();

            for (int i = 0; i < numberOfPoints; i++)
            {
                fan.MaxSpeed = retrievedValue[i];
            }
            return Task.FromResult(fan);
        }


        public Task<bool> TurnOn()
        {
            byte slaveId = 1;
            //12290
            ushort startAddress = 12290;
            ushort value = 300;
            ushort numberOfPoints = 1;

            _modbusMaster.WriteSingleRegister(slaveId, startAddress, value);
            ushort[] returnValue = _modbusMaster.
                ReadHoldingRegisters(slaveId, startAddress, numberOfPoints);
            if (returnValue[0] >= 300)
            {
                return Task.FromResult(true);
            }
            return Task.FromResult(false);
        }


        public Task<bool> TurnOff()
        {
            byte slaveId = 1;
            //12290
            ushort startAddress = 12290;
            ushort value = 0;
            ushort numberOfPoints = 1;

            _modbusMaster.WriteSingleRegister(slaveId, startAddress, value);
            ushort[] returnValue = _modbusMaster.
                ReadHoldingRegisters(slaveId, startAddress, numberOfPoints);
            if (returnValue[0] >= 300)
            {
                return Task.FromResult(true);
            }
            return Task.FromResult(false);
        }

    }
}