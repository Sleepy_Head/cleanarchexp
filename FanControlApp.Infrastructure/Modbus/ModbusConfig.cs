using System.IO.Ports;
using NModbus;
using NModbus.Serial;

namespace FanControlApp.Infrastructure
{
    public class ModbusConfig
    {

        public static IModbusMaster CreateRtuMaster (string portName)
        {

                var serialPort = new SerialPort(portName)
                {
                    BaudRate = 19200,
                    DataBits = 8,
                    Parity = Parity.Even,
                    StopBits = StopBits.One,
                };
                
                serialPort.Open();

                var facotry = new ModbusFactory();
             
                IModbusMaster master = facotry.CreateRtuMaster(
                    new SerialPortAdapter(serialPort)
                );

                return master;

        }
        
    }
}