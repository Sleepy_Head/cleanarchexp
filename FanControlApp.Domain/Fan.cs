﻿namespace FanControlApp.Domain;

public class Fan
{
  
    public string? Manufacturer { get; set; }
    public float DemandLevel { get; set; }
    public int CurentRMP { get; set; }
    public int MaxSpeed {get;set;}
    public int MaxCurrent { get; set; }

}
